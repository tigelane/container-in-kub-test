#! /usr/bin/env python3

import datetime
import os
import random
import time

dt_file_name = "./output.txt"

if os.path.exists(dt_file_name):
    dt_file = open(dt_file_name,'a')
else:
    dt_file = open(dt_file_name,'w')

for i in range(3):
    # datetext = f"The date and time is {datetime.datetime.now()}\n"
    dt_file.write(f"The date and time is {datetime.datetime.now()}\n")

    time.sleep (random.randint(0, 3))